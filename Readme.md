# Guardrails Home Assignments
## Part 1
### Question 1
We assume that  our services are deployed on EKS including Dashboard, API, Workers, ScanJob, Data Processing, Storage and Notification.
#### Improvements:
#### High availability:
    - We can apply autoscaling on EKS nodes and setup EKS nodes in multiple Availability Zone. 
    - Enable multiple replica in service deployments. We can also apply autoscale to deployments based on HPA (cpu,

    memory usage metrics) or custom metrics (api latency, queue length) as well https://keda.sh/ .

    - As for MQ, We can deploy RabbitMQ as a RabbitMQ cluster on EKS on separate nodes in different Availability Zone.
    - As for RDS, Primary and Replica instances are deployed in different Availability Zone. We can also consider Aurora PostgreSQL DB cluster.
#### Resilience:
    Resilience means that if incidents, failures happen the system still work properly and correctly.
    - Improve system availability as I mentioned above (redundancy, auto scaling, multiple AZ).
    - Load Test: Before applying new features on the live env, we should do load test to make sure the system is still alive under high load.
    - Can consider applying blue-green or canary deployment strategy.
    - Take advantage of Infrastructure as code.
        - We do not have to spend much time on repeatable tasks. 
        - It is easy to share and hand-over knowledge.
        - Easy to check what changed and history of infrastructure modification. 
    - Build a monitoring system and monitor necessary metrics such as service metrics ( latency, error rate),
    
     RabbitMQ metrics, DB metrics. When incidents happen, it is easy to find the root cause. 
    - Take advantage of distributed tracing to have a overview look at every component on the way requests go through. 
    - Apply circuit-breaking to API service to avoid cascading failures when incidents happen. 
    - Error Handling via Dead Letter Queue: If a message in the task queue was consumed, but the message cannot be processed correctly, then the message can be pushed to a Dead Letter Queue, and will be processed by another worker (workers for Dead Letter Queue only) later. A job process may die, make sure that the task will be executed by another job process.
    - Idempotence: A single task could be processed > 1 times, and make sure the correctness. 
    - implementing heartbeat & task status.
#### High Performance:
    - Consider implementing a cache layer in front of RDS DB, when data processing completed, then the result will be inserted/updated into db and cache layer at the same time, and make sure eventual consistency. We also have to implement cache invalidation logic to make sure data in cache and db is identical. 
    - If the Dashboard has many static files, consider using CDN to improve user experience. 
    - Is kafka a good replacement for RabbitMQ?
        - High performance (throughput, availability and consistency).
        - Using offset -> re-process messages with offset in case of after incidents/bugs.
        - Easy to scale consumers and producers horizontally with topic partitions and consumer groups.
        - We can also execute tasks based on task priority (implement separate queues for prioritized tasks).
        - we can ship the Scan results to a topic in Kafka, so it is easy for Data Processing service to read the scanning result. 
    - Monitor important metrics such as api latency, error rate, job processing time to find out the root cause, bottleneck. 
    - Internal services can communicate each other via GRPC instead of RESTFUL.
    - Minimize the docker images size of job to improve docker image download time.

#### Cost Efficiency
	- Use spot EKS nodes on non-live env. 
	- If want to verify the reliability, resilience and availability of the system, we could apply some eks spot node pools in order to monitor the system when some nodes are down. In my opinion, Spot node should not be applied for high critical and sensitive services (workers, jobs).
    - Adjust autoscaling (node and pod) Pay-as-you-use.

## Question 2
    As I mentioned above, we should implement autoscale for EKS node and Service Deployments based on Memory and CPU or other custom metrics.

    For example, Dashboard, Data Processing, Notification are IO-bound services, then apply autoscaling with Memory metrics. As for API service, we should consider latency metrics. Workers are cpu-bound apps.

    If we consider applying spot nodes (cost saving purposes) to services that are not too-high-risk and sensitive services (dashboard, api), we should consider graceful-shutdown-timeout (30 seconds), spot VMs terminate 30 seconds after receiving a termination notice, and terminationGracePeriodSeconds of Kubelet is 30s.

    So that when termination notice is sent to eks, our service should process SIGTERM in 30 seconds, make sure requests are not interrupted .
    
    Autoscaling based on metrics are fitting for random high-load traffic; however, if we have periodic tasks, periodic events we can try using https://www.rundeck.com/open-source to scale service periodically

## Question 3
    I think we can use storage solutions that support ReadWriteMany. Multiple Scan Job pods can mount the volume and use shared storage. Just need to pull repos one time.

    Of course, we need to implement clean-up jobs in order to clean no longer in use or deprecated repos.

    Technically, we can consider EFS as storage provider, with EFS we do not need to care about management and scalability.

    Another cheeper solution is NFS, we need to build our own NFS server, and pay effort on operation and management.


## Question 4
    Due to lack of detail of the question, so I have some general ideas below:

    To improve high-level disaster recovery ability, firstly, we need to guarantee the availability of the whole 
    
    system (as I mentioned in the Question 1 High availability and Resilience ).

    Apply chaos engineering to the system, prepare for all cases that probably happen (practice makes perfect).

    Consider building another system that is identical as the current system (quite high cost) in another region. In case of severe incidents happen, change dns records to the stand-by system. 

    Backup data daily is a good way to prevent data loss.

    -> when having more detail information, we will go deeper into solutions.





## Part 2
Job manifest files:
helm template -f helm-jobs/values.yaml helm-jobs

In order to meet the requirements ( the goal is to always schedule the jobs on the minimum amount of nodes) we are using pod affinity. Pod affinity can tell the scheduler to locate a new pod on the same node as other pods if the label selector on the new pod matches the label on the current pod.
Here we are using the label group-name: gr-jobs, so that pods which have the group-name: gr-jobs label, will likely to be deployed on the same node.

https://docs.okd.io/latest/nodes/scheduling/nodes-scheduler-pod-affinity.html

https://www.weave.works/blog/influencing-kubernetes-scheduler-decisions



	



